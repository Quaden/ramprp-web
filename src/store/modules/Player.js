const Player = {
	namespaced: true,
	state: {
		account: {
			id: process.env.VUE_APP_ENV === 'production' ? null : 1,
			login: process.env.VUE_APP_ENV === 'production' ? null : 'login',
		},
		character: {
			id: process.env.VUE_APP_ENV === 'production' ? null : 1,
			name: process.env.VUE_APP_ENV === 'production' ? null : 'character',
		}
	},
	mutations: {
		setAccount(state, account) {
			state.account = account;
		},
		setCharacter(state, character) {
			state.character = character;
		},
	},
	actions: {
		setAccount({commit}, account) {
			commit('setAccount', account);
		},
		setCharacter({commit}, character) {
			commit('setCharacter', character);
		},
	},
	getters: {
		account: state => state.account,
		character: state => state.character,
	},
};

export default Player;
