const House = {
	namespaced: true,
	state: () => ({
		house: {
			closed: null,
			owned: null,
			owner: {
				id: null,
				name: null,
			}
		},
	}),
	mutations: {
		setHouse(state, house) {
			state.house = house;
		},
	},
	actions: {
		setHouse({commit, rootState}, house) {
			commit('setHouse', house);
		},
	},
	getters: {
		house: (state, getters, rootState) => {
			let house = state.house;
			house.owned = house.owner && house.owner.id === rootState.Player.character.id;
			return house
		},
	},
};

export default House;
