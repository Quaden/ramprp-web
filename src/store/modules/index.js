/* base modules and misc*/
import App from './App';
import Auth from './Auth';
import Notifications from './misc/Notifications.js';
import House from './House';
import Player from "./Player";
/* account & caracters*/


const modules = {
	App,
	Auth,
	Notifications,
	House,
	Player,
};

export default modules;
