const App = {
	state: {
		validationErrors: {
			registration: {
				'email': '',
				'login': '',
			}
		},
		authData: {
			login: null,
			password: null,
			passwordConfirmation: null,
			promo: null,
			email: null,
			rules: false,
			rememberMe: false,
			passwordHash: null,
		}
	},
	mutations: {
		addValidationError(state, err) {
			let {page, field, error} = err;
			state.validationErrors[page][field] = [error];
		},
		setLogin(state, page) {
			state.authData.login = page;
		},
		setPassword(state, data) {
			state.authData.password = data;
		},
		setPasswordHash(state, data) {
			state.authData.passwordHash = data;
		},
		setRememberMe(state, data) {
			state.authData.rememberMe = data;
		}
	},
	actions: {
		addValidationError({commit}, err) {
			commit('addValidationError', err);
		},
		setLogin({commit}, page) {
			commit('setLogin', page);
		},
		setPassword({commit}, data) {
			commit('setPassword', data);
		},
		setPasswordHash({commit}, data) {
			commit('setPasswordHash', data);
		},
		setRememberMe({commit}, data) {
			commit('setRememberMe', data);
		}
	},
	getters: {
		getValidationErrors: state => state.validationErrors,
		getAuthData: state => state.authData
	}
};


export default App;
