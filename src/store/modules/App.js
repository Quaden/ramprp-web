const App = {
	state: {
		page: process.env.VUE_APP_ENV === 'production' ? null : 'house',
		action: process.env.VUE_APP_ENV === 'production' ? null : 'entrance',
		layout: process.env.VUE_APP_ENV === 'production' ? null : 'default',
	},
	mutations: {
		setPage(state, page) {
			state.page = page;
		},
		setAction(state, action) {
			state.action = action;
		},
		setLayout(state, data) {
			state.layout = data;
		},
	},
	actions: {
		setPage({commit}, page) {
			commit('setPage', page);
		},
		setAction({commit}, action) {
			commit('setAction', action);
		},
		setLayout({commit}, data) {
			commit('setLayout', data);
		},
	},
	getters: {
		page: state => state.page,
		layout: state => 'layout-' + state.layout,
		action: state => state.page + '-' + state.action
	},
};

export default App;
