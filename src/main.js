import Vue from 'vue'
import store from './store';
// import router from './router';
import { mapActions } from 'vuex';
import App from './components/App.vue'

/* demo */
// import axios from "axios";
// import VueAxios from "vue-axios";
// Vue.use(VueAxios, axios);

const app = new Vue({
	// router,
	store,
	render: h => h(App),
	methods: {
		...mapActions({
			'setPage':'setPage',
			'setLayout':'setLayout',
			'setLogin':'setLogin',
			'setAction': 'setAction',
			'setPassword':'setPassword',
			'setPasswordHash':'setPasswordHash',
			'setRememberMe':'setRememberMe',
			'addNotification':'addNotification',
			'addValidationError':'addValidationError',
			'setHouse':'House/setHouse',
			'setAccount':'Player/setAccount',
			'setCharacter':'Player/setCharacter',
		})
	}
}).$mount('#app');

export default app;
global.storage = app;
global.consoleAPI = function (data) {
	app.$store.dispatch("consolePush", JSON.stringify(data));
}
global.cef = app.$children[0];


// global.chatAPI = {
// 	push:(playerName, playerId, text, tag, value) =>{
// 		console.log('chatAPI push test NEED DELETE',playerName, playerId, text, tag, value);
// 	},
// 	show:(enable) =>{
// 		if(chatAPI.cacheChatStatus != enable){ 
// 			storage.setChatShow(enable); 
// 			chatAPI.cacheChatStatus = enable;
// 		}
// 	},
// 	cacheChatStatus: false,
// }

// console.info = function(...args) {
// 	const info = ['[CEF - info]'];
// 	info.push(...args);
// 	mp.trigger("console.push", "info",JSON.stringify(info.join(' ')));
// }

// console.log = function(...args) {
// 	const info = ['[CEF]'];
// 	info.push(...args);
// 	mp.trigger("console.push", "log",JSON.stringify(info.join(' ')));
// }

// console.warn = function(...args) {
// 	const info = ['[CEF - warn]'];
// 	info.push(...args);
// 	mp.trigger("console.push", "warn",JSON.stringify(info.join(' ')));
// }

// console.error = function(...args) {
// 	const info = [];
// 	info.push(...args);
// 	// mp.trigger("console.push", "error",JSON.stringify(info.join(' ')));
// 	mp.trigger("console:chrome", JSON.stringify({ timestamp: new Date().toISOString(), env: "cef", message: info.join(" "), level: "error" }));
// }
